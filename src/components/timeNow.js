import { Component } from "react";
let today = new Date();
let hours = today.getHours();
let minute = today.getMinutes();
let second = today.getSeconds();
let ampm = hours >= 12 ? "pm" : "am "

class TimeNow extends Component{
    constructor(props){
        super(props)

        this.state = {
            index: 1,
            hours: hours,
            minute: minute,
            second: second,
            ampm: ampm,
            time: []
        }
    }
    clickHandlerEvent = () => {
        let today = new Date();
        this.setState({
            index: this.state.index + 1,
            hours: today.getHours(),
            minute: today.getMinutes(),
            second: today.getSeconds(),
            ampm: this.state.hours >= 12 ? "pm" : "am ",
            time: [...this.state.time, this.state.index + ". " + this.state.hours + ":" + this.state.minute + ":" + this.state.second + " " + this.state.ampm]
        })
    }
    render(){
        return(
            <div>
                <p>List</p>

                <button onClick={this.clickHandlerEvent}>Add to list</button>
                
                {this.state.time.map((element, index) => {
                    return <p key={index}>{element}</p>
                })}
            </div>
        )
    }
}
export default TimeNow;